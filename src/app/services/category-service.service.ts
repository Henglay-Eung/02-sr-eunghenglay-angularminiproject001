import { Category } from './../interface/category-interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

    private baseUrl="http://localhost:8080"

  constructor(private httpClient:HttpClient) { }

  getAllCategories(){
    return this.httpClient.get(this.baseUrl+`/categories`)
  }

  getCategory(page,size){
    return this.httpClient.get(this.baseUrl+`/categories?sort=id,desc&page=${page}&size=${size}`)
  }

  postCategory(category:Category){
    return this.httpClient.post(this.baseUrl+"/categories",category)
  }

  getOneCategory(id:number){
    return this.httpClient.get(this.baseUrl+"/categories/"+id)
  }

  deleteCategory(id:number){
    return this.httpClient.delete(this.baseUrl+"/categories/"+id)
  }

  updateCategory(category:Category,id:number){
    return this.httpClient.put(this.baseUrl+"/categories/"+id,category)
  }
}
