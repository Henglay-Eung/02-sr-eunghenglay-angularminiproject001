import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from '../interface/book-interface';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private baseUrl="http://localhost:8080"

  constructor(private httpClient:HttpClient) { }

  getBook(page,size){
    return this.httpClient.get(this.baseUrl+`/books?sort=id,desc&page=${page}&size=${size}`)
  }

  postBook(book:Book){
    return this.httpClient.post(this.baseUrl+"/books",book)
  }

  deleteBook(id:number){
    return this.httpClient.delete(this.baseUrl+"/books/"+id)
  }

  getOneBook(id:number){
    return this.httpClient.get(this.baseUrl+"/books/"+id)
  }

  updateBook(book:Book,id:number){
    return this.httpClient.put(this.baseUrl+"/books/"+id,book)
  }

  searchBookByTitle(page,title:string){
    return this.httpClient.get(this.baseUrl+`/books/search/findByTitle?title=${title}&page=${page}&size=5`)
  }

  searchBookByCategoryTitle(page,categoryTitle:string){
    return this.httpClient.get(this.baseUrl+`/books/search/findByCategoryTitle?categoryTitle=${categoryTitle}&page=${page}&size=5`)
  }
}
