import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  private baseUrl="http://localhost:8080"

  constructor(private httpClient:HttpClient) { }

  uploadFile(file:File){
    const formData:FormData = new FormData()
    formData.append('file',file)
    return this.httpClient.post(this.baseUrl+"/upload",formData,{reportProgress:true})
  }
}
