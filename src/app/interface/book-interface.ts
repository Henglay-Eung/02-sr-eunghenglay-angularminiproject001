import { Category } from './category-interface';
export interface Book{
    id:number;
    author:string;
    description:string;
    thumbnail:string;
    title:string;
    category:Category;
}