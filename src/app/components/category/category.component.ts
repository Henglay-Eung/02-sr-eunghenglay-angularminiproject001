
import { CategoryService } from '../../services/category-service.service';
import { Component, OnInit } from '@angular/core';
import { Category } from '../../interface/category-interface';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  pageNo=1
  pageSize=5
  categories:Category[]
  totalCategories=0
  page:any
  closeResult=''
  categoryForm
  categoryToEdit:Category=null
  searchedText=""
  constructor(private categoryService:CategoryService,private modalService:NgbModal) { }

  //Get All Categories
  ngOnInit(): void {
    this.getAllCategories()
    this.categoryForm = new FormGroup({
      title:new FormControl('',[Validators.required])
    })
  }

  //Get All Categories Function
  getAllCategories(){
    this.categoryService.getCategory(this.pageNo-1,this.pageSize).subscribe(
      (response:any)=> {
        this.categories= response._embedded.categories
        this.totalCategories = response.page.totalElements
      }
    )
  }

  // Function For Opening Modal
  open(content) {
    this.categoryForm.get('title').value=''
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`; 
    }
  }

  //Submit Category Function
  onSubmit(){
    let category:Category ={} as Category
    category = this.categoryForm.value
    this.categoryService.postCategory(category).subscribe((response:any)=>{
      this.getAllCategories()
    })
  }

  //Delete Category Function
  deleteCategory(id){
    this.categoryService.deleteCategory(id).subscribe(res=>{
      this.getAllCategories()
    })
  }

  //Get Category To Edit Function
  viewCategoryToEdit(content,id){
    this.categoryService.getOneCategory(id).subscribe((res:Category)=>{
      this.categoryToEdit = res
      this.open(content)
      this.categoryForm.get("title").value = res.title
    })
  }

  //Submit Edited Category Function
  editCategory(id){
    let category:Category ={} as Category
    category = this.categoryForm.value
    this.categoryService.updateCategory(category,id).subscribe(res=>{
      this.getAllCategories()
    })
  }

  //Refresh Data When Move Page Function
  refreshCategories(){
    this.categoryService.getCategory(this.pageNo-1,this.pageSize).subscribe(
      (response:any)=> {
        this.categories = response._embedded.categories
      }
    )
  }
}
