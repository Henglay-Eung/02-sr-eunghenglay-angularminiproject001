import { UploadFileService } from './../../services/upload-file.service';
import { CategoryService } from './../../services/category-service.service';
import { Category } from '../../interface/category-interface';
import { BookService } from '../../services/book-service.service';
import { Component, OnInit } from '@angular/core';
import { Book } from '../../interface/book-interface';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';;

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent implements OnInit{

  image:File
  imageUrl
  imageUrlToEdit
  pageNo=1
  pageSize=5
  totalBooks=0
  allBooks : Book[]
  categories:Category[]
  closeResult=''
  bookForm
  bookToEdit:Book=null
  bookToView:Book=null
  searchByTitleText=''
  searchByCategoryTitleText=''

  constructor(private bookService:BookService,private modalService:NgbModal,
              private categoryService:CategoryService,private uploadFileService:UploadFileService) {  
              }

  //Get All Data
  ngOnInit(): void {
    this.getAllBooks()
    this.getAllCategories()
    this.bookForm = new FormGroup({
      title: new FormControl('',[Validators.required]),
      author: new FormControl('',[Validators.required]),
      description: new FormControl('',[Validators.required]),
      thumbnail: new FormControl(),
      category: new FormControl('',[Validators.required])
    })
  }

  //Function For Get All Categories
  getAllCategories(){
    this.categoryService.getAllCategories().subscribe(
      (response:any)=> {
        this.categories= response._embedded.categories
      }
    )
  }
  
  //Function For Get All Books
  getAllBooks(){

    if(this.searchByTitleText===''&&this.searchByCategoryTitleText===''){
      this.bookService.getBook(this.pageNo-1,this.pageSize).subscribe(
        (response:any)=> {
          this.allBooks = response._embedded.books
          this.totalBooks = response.page.totalElements
        }
      )
    }
    
    else if(this.searchByTitleText!==''){
      this.bookService.searchBookByTitle(this.pageNo-1,this.searchByTitleText).subscribe(
        (response:any)=>{
          this.allBooks = response._embedded.books
          this.totalBooks = response.page.totalElements
        }
      )
    }
    else{
      this.bookService.searchBookByCategoryTitle(this.pageNo-1,this.searchByCategoryTitleText).subscribe(
        (response:any)=>{
          this.allBooks = response._embedded.books
          this.totalBooks = response.page.totalElements
        }
      )
    }
   
  }

  //Function For Openning Modal
  open(content) {
 
    this.bookForm.get("title").setValue()
    this.bookForm.get("author").setValue()
    this.bookForm.get("description").setValue()
    this.bookForm.get("category").setValue()
    this.modalService.open(content, {size:'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.imageUrl = null
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.imageUrl = null
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  //Function For Load Image
  selectFile(event){
    
    this.image=event.target.files[0]
    var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
			this.imageUrl = reader.result; 
		}
  }

  //Function For Submit Books
  onSubmit(){
    let bookRequest = this.bookForm.value
    
    this.uploadFileService.uploadFile(this.image).subscribe((event:any)=>{
      bookRequest.category = {
        id: parseInt(this.bookForm.get("category").value) 
      }
      bookRequest.thumbnail = event.file
      this.bookService.postBook(bookRequest).subscribe(res=>{
        this.getAllBooks()   
      })
    })
    //this.image = undefined
    
  }

  //Function for delete book
  deleteBook(id){
    this.bookService.deleteBook(id).subscribe(res=>{
      this.getAllBooks()  
    })
  }
  
  //Function for get book to edit
  viewBookToEdit(content,id){
    this.bookService.getOneBook(id).subscribe((res:Book)=>{
      this.bookToEdit = res
      this.open(content)
      this.bookForm.get('title').setValue(res.title)
      this.bookForm.get('author').setValue(res.author)
      this.bookForm.get('description').setValue(res.description)
      this.bookForm.get('category').setValue(res.category.id)  
      this.imageUrlToEdit = res.thumbnail
    })  
  }

  //Function for submit editted book
  editBook(id){
    let bookEditRequest = this.bookForm.value
    bookEditRequest.category = {
      id: parseInt(this.bookForm.get("category").value) 
    }
    if(this.image!==null&&this.bookForm.get("thumbnail").value!==null){

      this.uploadFileService.uploadFile(this.image).subscribe((event:any)=>{   
        bookEditRequest.thumbnail = event.file
        this.bookService.updateBook(bookEditRequest,id).subscribe(res=>{
          this.getAllBooks()   
        })
      })
    }
    else{
      bookEditRequest.thumbnail = this.imageUrlToEdit
      this.bookService.updateBook(bookEditRequest,id).subscribe(res=>{
        this.getAllBooks()   
      })
    }

    this.image = null

  }

  //Function to view book
  viewBook(content,id){
    this.bookService.getOneBook(id).subscribe((res:Book)=>{
      this.bookToView = res
      this.open(content)   
    })
  }

  //Refresh data when move page fucntion
  refreshBooks(){
    if(this.searchByTitleText===''&&this.searchByCategoryTitleText==='')
    this.bookService.getBook(this.pageNo-1,this.pageSize).subscribe(
      (response:any)=> {
        this.allBooks = response._embedded.books
        this.totalBooks = response.page.totalElements
      }
    )
    else if(this.searchByTitleText!==''){
      this.bookService.searchBookByTitle(this.pageNo-1,this.searchByTitleText).subscribe(
        (response:any)=>{
          this.allBooks = response._embedded.books
          this.totalBooks = response.page.totalElements
        }
      )
    }
    else{
      this.bookService.searchBookByCategoryTitle(this.pageNo-1,this.searchByCategoryTitleText).subscribe(
        (response:any)=>{
          this.allBooks = response._embedded.books
          this.totalBooks = response.page.totalElements
        }
      )
    }
  }

  //Function for searching books by title
  searchByTitle(event){
    this.searchByTitleText = event.target.value
    if(event.target.value === "")
      this.getAllBooks()
    else
      this.bookService.searchBookByTitle(this.pageNo-1,event.target.value).subscribe(
      (response:any)=>{
        this.allBooks = response._embedded.books
        this.totalBooks = response.page.totalElements
      }
    )
  }

  //Function for filtering books by category title
  searchByCategoryTitle(event){
    this.searchByCategoryTitleText=event.target.value
    if(event.target.value === "")
      this.getAllBooks()
    else  
      this.bookService.searchBookByCategoryTitle(this.pageNo-1,event.target.value).subscribe(
      (response:any)=>{
        this.allBooks = response._embedded.books
        this.totalBooks = response.page.totalElements
      }
    )
  }
}
