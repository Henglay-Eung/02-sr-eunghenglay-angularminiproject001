
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from '../components/category/category.component';
import { HomepageComponent } from '../components/homepage/homepage.component';

const arrRoute: Routes = [
  {
    path:"",
    component:HomepageComponent
  },
  {
    path:"category",
    component:CategoryComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(arrRoute)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
